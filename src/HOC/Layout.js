import { Box } from "@mui/material";
import React from "react";
import Header from "../Components/Header/Header";

export default function Layout({ Component }) {
  return (
    <>
     <Header />
     <Component />
    </>
  );
}
