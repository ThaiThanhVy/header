import logo from "./logo.svg";
import "./App.css";
// npm i ant design
import "antd/dist/antd.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SecureView from "./HOC/SecureView";
import Layout from "./HOC/Layout";
import HomePage from "./Page/HomePage/HomePage";
import LoginPage from "./Page/LoginPage/LoginPage";
import Spinner from "./Components/Spinner/Spinner";
import RoomDetail from "./Page/RoomDetail/RoomDetail";
import { Box } from "@mui/material";
import BecomeAHost from "./Page/BecomeAHost/BecomeAHost";
import SignUpPage from "./Page/SignUpPage/SignUpPage";
import UserInfor from "./Page/UserInforPage/UserInfor";
import Header from "./Components/Header/Header";
import DashBoard from "./Page/Admin/DashBoard/DashBoard";
import User from "./Page/Admin/User/User";
function App() {
  return (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
      height: '100vh'
    }}>
      <Box>
        <Spinner />
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={
                <SecureView>
                  <Layout Component={HomePage} />
                </SecureView>
              }
            />
            <Route path="/becomeahost" element={<BecomeAHost />} />
            <Route
              path="/roomdetail/:id"
              element={
                <SecureView>
                  <Layout Component={RoomDetail} />
                </SecureView>
              }
            />
            <Route path="/userinfor" element={<Layout Component={UserInfor} />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/signup" element={<SignUpPage />} />
            {/* <Route path="/das" element={<DashBoard />} /> */}
            <Route path="/adminuser" element={<DashBoard Component={User} />} />
          </Routes>
        </BrowserRouter>
      </Box>
    </Box>

  );
}

export default App;
