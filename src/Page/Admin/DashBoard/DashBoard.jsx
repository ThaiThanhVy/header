import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Breadcrumb, Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { localServ, USER } from '../../../Services/LocalService';
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  getItem('User', 'sub1', <UserOutlined />, [
    getItem('Tom', '3'),
    getItem('Bill', '4'),
    getItem('Alex', '5'),
  ]),
  getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
  getItem('Files', '9', <FileOutlined />),
];
const DashBoard = (props) => {
  let user = useSelector((state) => {
    return state.reducerUser.userInfor;
  });
  let { role, name } = user.user

  if (!localStorage.getItem(USER)) {
    alert("Bạn Không có quyền truy Cập vào trang này");
    return (window.location.href = "/");
  }


  if (role !== "ADMIN") {
    alert("Bạn không có quyền");
    return (window.location.href = "/");
  }

  let handleRemove = () => {
    localServ.user.remove();
    setTimeout(() => {
      window.location.href = "/";
    }, 1000);
  };


  let opaerations = () => {
    if (user) {
      return (
        <div className="flex justify-end mt-2">
          <NavLink to={"/userinfor"}>
            <button
              id="logo_Hoten"
              style={{
                width: 60,
                height: 60,
                backgroundColor: "rgba(191, 62, 129, 0.8)",
              }}
              className=" font-medium rounded-full opacity-100 text-white mr-7 "
            >
              {name.substr(0, 1)}
            </button>
          </NavLink>


          <button
            style={{ padding: "0px 7px" }}
            onClick={() => {
              handleRemove();
            }}
            className="font-semibold rounded text-xl dark:bg-violet-400 text-white bg-red-400 hover:text-neutral-900"
          >
            Đăng Xuất
          </button>
        </div>
      );
    }
  };

  let { Component } = props;
  const [collapsed, setCollapsed] = useState(false);
  return (
    <Layout
      style={{
        minHeight: '100vh',
      }}
    >
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div className=" bg-opacity-40 flex justify-between h-16 mx-auto">
          <div>
            <a
              rel="noopener noreferrer"
              href="#"
              aria-label="Back to homepage"
              className="flex items-center animate-bounce"
            >
              <NavLink to={"/"}>
                <h3 style={{ color: '#FF385C' }} className="w-8 h-8  text-3xl mt-3 ml-3">
                  Airbnb
                </h3>
              </NavLink>
            </a>
          </div>

        </div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background  "
          style={{
            paddingBottom: 80,
          }}
        >
          {opaerations()}
        </Header>
        <Content
          style={{
            margin: '0 16px',
          }}
        >
          <Breadcrumb
            style={{
              margin: '16px 0',
            }}
          >
            <Breadcrumb.Item>Quản lí Người Dùng</Breadcrumb.Item>
          </Breadcrumb>
          <div
            className="site-layout-background"
            style={{
              padding: 24,
              minHeight: 360,
            }}
          >
            <Component />
          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
          }}
        >
        </Footer>
      </Layout>
    </Layout>
  );
};
export default DashBoard;