import axios from "axios";
import { store } from "../index";
import { setLoadingOnAction } from "../redux/actions/actionSpinner";
import { localServ } from "./LocalService";

export const BASE_URL = "https://airbnbnew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNCIsIkhldEhhblN0cmluZyI6IjA1LzAzLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Nzk3NDQwMDAwMCIsIm5iZiI6MTY1NDEwMjgwMCwiZXhwIjoxNjc4MTIyMDAwfQ.FunqYipkHrCbBATBzuJXyjGpZZxDekx1oY2qxW3_yfw"
export let https = axios.create({
  baseURL: BASE_URL,
  headers: {
    token: localServ.user.get()?.token,
    tokenCybersoft: TOKEN_CYBERSOFT,
  },
});

