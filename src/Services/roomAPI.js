import { https } from "./configURL";

export const roomAPI = {
  getListRoom: () => {
    let uri = `/api/phong-thue`;
    return https.get(uri);
  },
  getInforRoom: (maNguoiDung) => {
    let uri = `/api/dat-phong/lay-theo-nguoi-dung/${maNguoiDung}`;
    return https.get(uri);
  },
  getRentedRoom: (id) => {
    let uri = `/api/phong-thue/${id}`;
    return https.get(uri);
  },
};
