import { INFOR_ROOM, RENTED_ROOM } from "../constants/constantsRoom";

let initalState = {
  roomDetail: null,
  listRoom: [],
  inforRoom: [],
  rentedRoom: [],
};

export const roomReducer = (state = initalState, action) => {
  switch (action.type) {
    case "LIST_ROOM": {
      state.listRoom = action.listRoom;
      return { ...state };
    }
    case INFOR_ROOM: {
      state.inforRoom = action.inforRoom;
      return { ...state };
    }
    case RENTED_ROOM: {
      state.rentedRoom = action.rentedRoom;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
