import { combineReducers } from "redux";
import { roomReducer } from "./reducerRoom";
import { spinnerReducer } from "./reducerSpinner";
import { reducerUser } from "./reducerUser";
export let rootReducer = combineReducers({ reducerUser, spinnerReducer, roomReducer });

