import { message } from "antd";
import { roomAPI } from "../../Services/roomAPI";
import { INFOR_ROOM, RENTED_ROOM } from "../constants/constantsRoom";
import { setLoadingOffAction, setLoadingOnAction } from "./actionSpinner";

export const getListRoomAction = () => {
  return async (dispatch) => {
    try {
      dispatch(setLoadingOnAction());

      const result = await roomAPI.getListRoom();

      dispatch({
        type: "LIST_ROOM",
        listRoom: result.data.content,
      });
      dispatch(setLoadingOffAction());
    } catch (err) {
      message.error(err.response?.data);
      dispatch(setLoadingOffAction());
    }
  };
};


export const getInforRooms = (maNguoiDung) => {
  return async (dispatch) => {
    try {
      dispatch(setLoadingOnAction());

      const result = await roomAPI.getInforRoom(maNguoiDung);
      // console.log('result: ', result);
      dispatch({
        type: INFOR_ROOM,
        inforRoom: result.data.content,
      });
      dispatch(setLoadingOffAction());
    } catch (err) {
      message.error(err.response?.data);
      dispatch(setLoadingOffAction());
    }
  };
};


export const getListRentedRooms = (maPhong) => {
  return async (dispatch) => {
    try {
      dispatch(setLoadingOnAction());

      const result = await roomAPI.getRentedRoom(maPhong);
      // console.log('result render room: ', result);
      dispatch({
        type: RENTED_ROOM,
        rentedRoom: result.data.content,
      });
      dispatch(setLoadingOffAction());
    } catch (err) {
      message.error(err.response?.data);
      dispatch(setLoadingOffAction());
    }
  };
};

